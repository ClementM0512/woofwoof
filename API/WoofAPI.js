import axios from 'axios';

export function getWoofs(){
    const url = 'http://chienmix.ml/api/chiens'
    return new Promise((resolve, reject) => {
        return axios.get(url).then(response => {
            resolve(response)
        }).catch(error => {
            reject(error)
        });
    });
}

