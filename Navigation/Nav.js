import { createAppContainer } from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import Acceuil from '../Component/Acceuil';
import AffWoofMix from '../Component/AffWoofMix';
import ListeWoof from '../Component/ListeWoof';

const woofStackNavigator = createStackNavigator({
    Acceuil:{
        screen: Acceuil,
        navigationOptions: {
            title: 'Woof Woof app',
            headerStyle:{
                backgroundColor: "#33CFFF",
            },
            headerTitleStyle:{
                textAlign: "center",
            }
        },
    },
    AffWoofMix:{
        screen: AffWoofMix,
        navigationOptions: {
            title: 'Woof Woof app',
            headerStyle:{
                backgroundColor: "#33CFFF",
            },
            headerTitleStyle:{
                textAlign: "center",
            }
        },
    },
    ListeWoof:{
        screen: ListeWoof,
        navigationOptions: {
            title: 'Woof Woof app',
            headerStyle:{
                backgroundColor: "#33CFFF",
            },
            headerTitleStyle:{
                textAlign: "center",
            }
        },
    },
})
export default createAppContainer(woofStackNavigator)