import React from 'react';
import { View, Text, StyleSheet, Image, Button } from 'react-native'

class AffWoofMix extends React.Component {
        constructor(props){
            super(props)
        }

        _woofMaleList(){
            this.props.navigation.navigate(
                'ListWoof'
                )
        }

        render() {
        const woofMale = this.props.navigation.state.params.woofId
        return (
            <View style={styles.container}>
                <View style={styles.header}></View>
                <View style={styles.SelectMenu}>
                    <View style={styles.SelectWoof}>
                        <Text style={styles.TxtWoof}>Sélection d'un male</Text>
                        <View  style={styles.BtnSelectWoof}>
                            <Button title='Sélectionner un chien'
                               onPress={() => this._woofMaleList()} />
                        </View>
                    </View>
                    <View style={styles.SelectWoof}>
                    <Text style={styles.TxtWoof}>Sélection d'une femelle</Text>
                        <View  style={styles.BtnSelectWoof}>
                            <Button title='Sélectionner un chien'
                                />
                        </View>
                    </View>
                </View>
                <View style={styles.footer}>
                    <View style={styles.Btnsfooter}>
                        <View  style={styles.BtnAnnuler}>
                            <Button title='Annuler' color='red'
                                />
                        </View>
                        <View  style={styles.BtnReinit}>
                            <Button title='Réinitialiser'
                                />
                        </View>
                        <View  style={styles.BtnValider}>
                            <Button title='Valider' color='green'
                                />
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    SelectWoof:{
        flex: 3,
    },
    TxtWoof:{
        textAlign: "center",
        fontSize: 20,
        fontWeight: "bold",
    },
    SelectMenu:{
        alignItems: "center",
        justifyContent: "center",
        flex: 9,
    },
    Btnsfooter:{
        flexDirection: "row",
        justifyContent: "space-around"
    },
    header:{
        flex: 1,
    },
    footer:{
        flex: 1,
    },
    BtnSelectWoof: {
        margin: 5,
    },
    container: {
        flex: 1   
    },
})
export default AffWoofMix



