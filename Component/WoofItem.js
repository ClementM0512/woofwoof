import { View, Text, StyleSheet, Image, Button } from "react-native";
import React from 'react';

class WoofItem extends React.Component {
    constructor(props){
        super(props)
        this.state = {isDetailsDisplayed: false}
    }
    _toggleDisplayDetails(){
        this.setState({isDetailsDisplayed: !this.state.isDetailsDisplayed})
        console.log(this.state.isDetailsDisplayed)
    }
    _displayDetails(){
        const chien = this.props.chien
        if (this.state.isDetailsDisplayed){
            return(
                <View style={styles.woofDetails}>
                    <View style={styles.woofDesc}>
                        <Text style={styles.woofDescText}>{chien.desc}</Text>
                    </View>
                    <View style={styles.woofProprietes}>
                    <View style={styles.woofCouleur}>
                            <Text style={styles.woofCouleurText}>{chien.color}</Text>
                        </View>
                        <View style={styles.woofCarac}>
                            <Text style={styles.woofCaracText}>{chien.character}</Text>
                        </View>
                    </View>
                </View>
                
            )
        }
    }
    render() {
        const chien = this.props.chien
        return (
            <View>
                <View style={styles.woofItem}>
                    <Image style={styles.image} source={{uri: chien.imageUrl}}></Image>
                    <View style={styles.woofInfo}>
                        <View style={styles.woofName}>
                            <Text style={styles.woofNameText}>{chien.name}</Text>
                        </View>
                        <View  style={styles.woofBtnInfo}>
                            <Button title='+ INFOS'
                                onPress={() => this._toggleDisplayDetails()}/>
                        </View>
                        <View  style={styles.woofBtnSelect}>
                            <Button title='Select'
                                onPress={() => this.props.backToSelect(chien._id)}/>
                        </View>
                    </View>
                </View>
                <View style={styles.woofItemInfo}>
                {this._displayDetails()}
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    woofBtnSelect:{
        marginTop: 5,
        color: 'green',
    },
    woofDetails:{
        margin: 5,
    },
    woofItemInfo:{
        flex: 1,
    },
    woofDesc: {
        flex: 2
    },
    woofDescText : {
        // color: 'gray'
     },
     woofProprietes: {
        flex: 2,
        flexDirection: 'row'
    },
    woofCouleur: {
        flex: 1,
        color: 'gray'
    },
    woofCarac: {
        flex: 1,
    },
    woofCouleurText: {
        color: 'gray'
    },
    woofCaracText: {
        color: 'gray'
    },
    woofBtnInfo:{
        flex: 1,
        justifyContent: 'center',
    },
    woofItem : {
        flexDirection: 'row',
        backgroundColor: '#e3e3e3',
        margin: 5
    },
    image: {
        width: 100,
        height: 100,
        margin: 5,
        backgroundColor: 'gray'
    },
    woofInfo: {
        flex: 1,
        flexDirection: 'column',
    },
    woofName: {
        flex: 1,
        justifyContent: 'center',
    },
    woofNameText: {
        textAlign: 'center',
        fontSize: 18,
        fontWeight: 'bold'
    },
  });
export default WoofItem;