import { View, Text, StyleSheet, Picker, Image } from "react-native";
import React from 'react';

class Woof extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.text}>Male</Text>
                <Picker>
                    <Picker.Item value='toto' label='toto'/>
                    <Picker.Item value='titi' label='titi'/>
                    <Picker.Item value='grosMinet' label='grosMinet'/>
                </Picker>  
                <Text style={styles.text}>Female</Text>
                <Picker>
                    <Picker.Item value='toto' label='toto'/>
                    <Picker.Item value='titi' label='titi'/>
                    <Picker.Item value='grosMinet' label='grosMinet'/>
                </Picker> 
                <Text style={styles.text}>Résultat du mélange:</Text>
                <Image style={styles.img} source={{uri:'https://lh3.googleusercontent.com/proxy/M2_bFeC7B9rwDIRxAjdBOoa2gVjnwbyyoogr-RKcw30_71Mut298Arc9F5EB5xLxppnbP2_u_DgSPw-AcFb7xiVFS5MYmISeHSt6sh4sSZD0QP7SdLnUztEoHOXhFC0'}}/>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    img:{
        marginTop: 35,
        width: 380,
        height: 350,
    },
    text:{
        backgroundColor: '#A7DAEA',
        fontSize: 15,
        paddingBottom: 10,
        paddingTop: 10,
        textAlign: 'center',
        fontWeight: "bold",
    },
    container: {
        flex:1,
    },
  });
export default Woof;