import React from 'react';
import { View, Text, StyleSheet, Image } from 'react-native'

class ChienItem extends React.Component {
    render() {
        const chien = this.props.chien
        return (
            <View style={styles.chienItem}>
                <Image style={styles.image} source={{uri: chien.imageUrl}}></Image>
                <View style={styles.chienInfo}>
                    <View style={styles.chienName}>
                        <Text style={styles.chienNameText}>{chien.name}</Text>
                    </View>
                    <View style={styles.chienDesc}>
                        <Text style={styles.chienDescText}>{chien.desc} </Text>
                    </View>
                    <View style={styles.chienProprietes}>
                        <View style={styles.chienCouleur}>
                            <Text style={styles.chienCouleurText}>{chien.color}</Text>
                        </View>
                        <View style={styles.chienCarac}>
                            <Text style={styles.chienCaracText}>{chien.character}</Text>
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    chienItem : {
        flexDirection: 'row',
        backgroundColor: '#e3e3e3',
        margin: 5
    },
    image: {
        width: 100,
        height: 100,
        margin: 5,
        backgroundColor: 'gray'
    },
    chienInfo: {
        flex: 1,
        margin: 2,
        flexDirection: 'column'
    },
    chienName: {
        flex: 1
    },
    chienDesc: {
        flex: 2
    },
    chienNameText: {
        textAlign: 'center',
        fontSize: 18,
        fontWeight: 'bold'
    },
    chienDescText : {
       // color: 'gray'
    },
    chienProprietes: {
        flex: 2,
        flexDirection: 'row'
    },
    chienCouleur: {
        flex: 1,
        color: 'gray'
    },
    chienCarac: {
        flex: 1,
    },
    chienCouleurText: {
        color: 'gray'
    },
    chienCaracText: {
        color: 'gray'
    }
})
export default ChienItem



