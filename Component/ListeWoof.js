import { View, Text, StyleSheet, Image, FlatList } from "react-native";
import React from 'react';
import WoofItem from './WoofItem';
import {getWoofs} from '../API/WoofAPI';

class ListWoof extends React.Component {
    constructor(props){
        super(props)
        this.state = {_woofs: []}
    }
    componentDidMount(){
        getWoofs().then(res =>{
            this.setState({_woofs: res.data})            
        }).catch(err =>{
            console.log(err)
        })                
    }
    _backToSelect = (idWoof) => {
        this.props.navigation.navigate(
            'AffWoofMix',
            {woofId: idWoof}
            )
    }
    render() {
        return (
            <View style={styles.container}>
                <FlatList data={this.state._woofs}
                    keyExtractor={(item) => item._id}
                    renderItem={({item}) => <WoofItem chien={item} backToSelect={this._backToSelect}></WoofItem>}>
                </FlatList>
                
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
    },
  });
export default ListWoof;