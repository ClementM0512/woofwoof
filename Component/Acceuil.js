import React from 'react';
import { View, Text, StyleSheet, Image, Button } from 'react-native'

class Acceuil extends React.Component {
    constructor(props){
        super(props)
    }
    _AffichageMix(){
        this.props.navigation.navigate(
            'AffWoofMix'
            )
    }

    _AjoutMix(){
        
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.Menu}> 
                    <Image style={styles.image} source={require("../assets/logoWoof.png")}/>
                    <View style={styles.Btns}>
                        <View  style={styles.BtnAffMix}>
                            <Button title='Afficher un mélange'
                                onPress={() => this._AffichageMix()}/>
                        </View>
                        <View  style={styles.BtnAddMix}>
                            <Button title='Ajouter un mélange'
                                onPress={() => this._AjoutMix()}/>
                        </View>
                    </View>
                </View>
                <View style={styles.footer}></View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    footer:{
        flex: 2,
    },
    Btns: {
    },
    title: {
        flex: 1
    },
    BtnAffMix: {
        margin: 4,
    },
    BtnAddMix: {
        margin: 4,
    },
    Menu: {
        alignItems: "center",
        justifyContent: "center",
        flex: 9,
    },
    container: {
        flex: 1,
    },
    image: {
        width: 150,
        height: 150,
        margin: 5,
        backgroundColor: 'gray',
    },    
})
export default Acceuil



