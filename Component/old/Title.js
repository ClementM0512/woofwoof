import { View, Text, StyleSheet } from "react-native";
import React from 'react';

class TitleApp extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.text}>Woof App</Text>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    text: {
        fontSize: 25,
        color: 'white',
        marginTop: 20,
    },
    textColor:{
        color: 'white',
        marginBottom: 10,

    },
    container:{
        alignItems: 'center',
        backgroundColor: '#33CFFF',
    }
  });
export default TitleApp