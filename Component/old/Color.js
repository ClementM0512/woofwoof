import { View, Text, StyleSheet } from "react-native";
import React from 'react';

class Color extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={{flex:1, backgroundColor:'purple'}}></View>
                <View style={{flex:9, backgroundColor:'blue', padding: 100, flexDirection: 'row'}}>
                    <View style={{flex:1, backgroundColor:'green'}}></View>
                    <View style={{flex:1, backgroundColor:'yellow'}}></View>
                </View>
                <View style={{flex:1, backgroundColor:'blue'}}></View>
                <View style={{flex:1, backgroundColor:'red'}}></View> 
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex:1,
    },
  });
export default Color