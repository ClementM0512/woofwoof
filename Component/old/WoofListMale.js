import { View, Text, StyleSheet, Image, FlatList } from "react-native";
import React from 'react';
import WoofItem from './WoofItem';
import {getWoofs} from '../API/WoofAPI';

class WoofListMale extends React.Component {
    constructor(props){
        super(props)
        this.state = {_woofs: []}
    }
    componentDidMount(){
        getWoofs().then(res =>{
            this.setState({_woofs: res.data})            
        }).catch(err =>{
            console.log(err)
        })                
    }
    _goToFemale = (idWoof) => {
        this.props.navigation.navigate(
            'WoofListFemale',
            {maleId: idWoof}
            )
    }
    render() {
        return (
            <View style={styles.container}>
                <FlatList data={this.state._woofs}
                    keyExtractor={(item) => item._id}
                    renderItem={({item}) => <WoofItem chien={item} goToFemale={this._goToFemale}></WoofItem>}>
                </FlatList>
                
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
    },
  });
export default WoofListMale;