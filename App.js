import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import Nav from './Navigation/Nav';
import Acceuil from './Component/Acceuil';

export default function App() {
  return (
      <Nav/>
  );
}

const styles = StyleSheet.create({
  title:{
    flex: 1,
  },
  body:{
    flex:7,
  },
  container: {
  },
});
