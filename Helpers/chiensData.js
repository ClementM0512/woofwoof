// Helpers/chiensData.js

const data = [{
      id: 1,
      name: "Labrador",
      desc: "Le retriever du Labrador, plus communément appelé labrador retriever ou plus simplement labrador, est une race de chiens originaire du Royaume-Uni. ",
      color: 'Noire, marron ou jaune',
      character: '',
      imageUrl: 'https://lh3.googleusercontent.com/proxy/M2_bFeC7B9rwDIRxAjdBOoa2gVjnwbyyoogr-RKcw30_71Mut298Arc9F5EB5xLxppnbP2_u_DgSPw-AcFb7xiVFS5MYmISeHSt6sh4sSZD0QP7SdLnUztEoHOXhFC0'
   },
   {
      id: 2,
      name: "Basset fauve de Bretagne",
      desc: "Le basset fauve de Bretagne est une race de chien de chasse originaire de Bretagne. ",
      color: 'Fauve unicolore.',
      character: 'Sociable, affectueux, équilibré.',
      imageUrl: 'https://ih1.redbubble.net/image.1142398341.0837/fposter,small,wall_texture,product,750x1000.jpg'
   },
   {
      id: 3,
      name: "Beagle",
      desc: "Le beagle  est une race de chien originaire d’Angleterre, de taille petite à moyenne.",
      color: 'Uni (blanc), bicolore (blanc et sable) ou tricolore (blanc, noir et marron).',
      character: 'Amical et attachant.',
      imageUrl: 'https://pbs.twimg.com/profile_images/1237689454908841985/F7rKgxaD_400x400.jpg'
   }
]

export default data